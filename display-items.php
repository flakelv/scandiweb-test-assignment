<?php
include "config.php";
$sql = "SELECT * FROM items";
$query = $dbh->prepare($sql);
$query->execute();
$results = $query->fetchAll(PDO::FETCH_OBJ);
sort($results);
$cnt = 1;
?>
<div class="container">
    <?php if ($query->rowCount() > 0) {
        foreach ($results as $result) {
            include_once "items.php";
            $product = null;
            switch ($result->type) {
                case 'DVD':
                    $product = new DVD($result->sku, $result->name, $result->price, $result->size);
                    $product->setSize(htmlentities($result->size));
                    break;
                case 'Furniture':
                    $product = new Furniture($result->sku, $result->name, $result->price, $result->height, $result->width, $result->length);
                    $product->setHeight(htmlentities($result->height));
                    $product->setWidth(htmlentities($result->width));
                    $product->setLength(htmlentities($result->length));
                    break;
                case 'Book':
                    $product = new Book($result->sku, $result->name, $result->price, $result->weight);
                    $product->setWeight(htmlentities($result->weight));
                    break;
                default:
                    break;
            }

            $product->setSKU(htmlentities($result->sku));
            $product->setName(htmlentities($result->name));
            $product->setPrice(htmlentities($result->price));
            ?>
            <div class="column">
                <div class="card">
                    <div class="card-body">
                        <input type="checkbox" name="delete-check[]" class="delete-checkbox" value="<?= $result->sku;?>">
                        <br>
                        <h5 class="card-title"><?php echo $product->getName();?></h5>
                        <p class="card-text"><?php echo $product->getSku();?>
                            <br><?php echo $product->getPrice(). " $";?>
                            <br><?php $type = $product->getType();
                                        if($type=="DVD"){
                                           echo "Size: " . $product->getSize();
                                        }else if($type=="Furniture"){
                                           echo "Dimensions: " . $product->getHeight() . "x" . $product->getWidth() . "x" . $product->getLength();
                                        }else if($type=="Book"){
                                           echo "Weight: " . $product->getWeight();
                                        }
                                    if($type=="DVD"){
                                        echo " MB";
                                    }else if($type=="Book"){
                                        echo " KG";
                                    }
                                    ?>
                        </p>
                        <br>
                    </div>
                </div>
            </div>
            <?php $cnt = $cnt + 1;
        }
    }
    ?>
</div>



<style>
    .container {
        display: grid;
        grid-template-columns: repeat(4, 1fr);
        align-items: center;
        gap: 10px;
    }
    .card {
        text-align: center;
        align-items: center;
        border-style: solid;
        border-color: #7c7c7c;
        margin-bottom: 20px;
        margin-right: 10px;
    }
    .card-body {
        margin: 10px;
        width: 250px;
    }
    .delete-checkbox {
        display: flex;
        float: left;
    }
</style>