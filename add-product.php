<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="index.css">
    <script src="index.js"></script>
    <title>Document</title>
</head>
<body>
<main-content>
<header>
    <h2>Product Add</h2>
</header>
<nav>
    <form action="index.php">
        <button type="submit" class="btn btn-primary" form="product_form">Save</button>
    </form>
    <form action="index.php">
    <button type="submit" class="btn btn-danger">Cancel</button>
    </form>
</nav>
<hr>
<section>
    <form action="insert.php" method="post" id="product_form" autocomplete="off">
        <div class="info">
            <label>SKU</label>
            <input type="text" id="sku" name="sku" maxlength="20" required/>
            <br>

            <label>Name</label>
            <input type="text" id="name" name="name" maxlength="30" required/>
            <br>

            <label>Price (In $)</label>
            <input type="number" step="any" id="price" name="price" maxlength="11" required/>
            <br>

            <label for="productType">Type switcher</label>
            <select name="productType" id="productType" onchange="selector()" required>
                <option value="" disabled selected>--Please choose an option--</option>
                <option value="DVD" id="DVD">DVD-disc</option>
                <option value="Furniture" id="Furniture">Furniture</option>
                <option value="Book" id="Book">Book</option>
            </select>
            <br><br>
            <div id="select-1">
                <label>Size(MB)</label>
                    <input type="number" id="size" name="size"/>
                    <br>
                <p>*Please provide the disc size in MB</p>
            </div>
            <div id="select-2">
                <label>Height(CM)</label>
                    <input type="number" min="1" step="any" id="height" name="height"/>
                    <br>
                <label>Width(CM)</label>
                    <input type="number" min="1" step="any" id="width" name="width"/>
                    <br>
                <label>Length(CM)</label>
                     <input type="number" min="1" step="any" id="length" name="length"/>
                     <br>
                <p>*Please provide the dimensions in CM.</p>
            </div>
            <div id="select-3">
                <label>Weight(KG)</label>
                    <input type="number" step="any" id="weight" name="weight"/>
                    <br>
                <p>*Please provide the weight of the item in KG.</p>
            </div>
        </div>
    </form>
</section>
</main-content>
<?php include "footer.php";?>
</body>
</html>