function selector() {
    var selection = document.getElementById("productType").value;

    if (selection == "DVD") {
        document.getElementById("size").required = true;
        document.getElementById("height").required = false
        document.getElementById("width").required = false;
        document.getElementById("length").required = false;
        document.getElementById("weight").required = false;
        document.getElementById("select-3").style.display = "none";
        document.getElementById("select-2").style.display = "none";
        document.getElementById("select-1").style.display = "block";
    }

    if (selection == "Furniture") {
        document.getElementById("height").required = true;
        document.getElementById("width").required = true;
        document.getElementById("length").required = true;
        document.getElementById("weight").required = false;
        document.getElementById("size").required = false;
        document.getElementById("select-3").style.display = "none";
        document.getElementById("select-2").style.display = "block";
        document.getElementById("select-1").style.display = "none";
    }
    if (selection == "Book") {
        document.getElementById("weight").required = true;
        document.getElementById("height").required = false;
        document.getElementById("width").required = false;
        document.getElementById("length").required = false
        document.getElementById("size").required = false;
        document.getElementById("select-3").style.display = "block";
        document.getElementById("select-2").style.display = "none";
        document.getElementById("select-1").style.display = "none";
    }
}
