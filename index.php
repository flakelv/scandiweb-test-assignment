<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="index.css">
    <script src="index.js"></script>
    <title>Scandiweb</title>
</head>
<body>
<main-content>
<header>
    <h2>Product List</h2>
</header>
    <nav>
        <form action="add-product.php">
            <button type="submit" class="btn btn-primary">ADD</button>
        </form>
        <button form="delete-items" type="submit" id="delete-product-btn" name="delete-product-btn" class="btn btn-danger">MASS DELETE</button>
    </nav>
        <form method="POST" action="delete-product.php" id="delete-items">
            <hr>
                <div class="main">
                    <?php
                    include "display-items.php";
                    ?>
                </div>
        </form>
        <?php
        include "connection.php";
        ?>
</main-content>
<?php include "footer.php";?>
</body>
</html>