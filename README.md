## Product management system
A simple product management system with the possibility to add new DVD's, Books and Furniture by type, while also adding their specific attributes.

## Prerequisites
WAMP | XAMPP etc.

## Installation
- Clone this repository to your computer.
- Setup your local server.
- Create a new database or use an existing one and change details in 'connection.php' and 'config.php'.
- To populate your database you can use 'seeder.php' by launching it in a seperate tab. (Also change details in seeder.php)

## Author
Aleks Grišins