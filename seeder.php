<?php
/* Attempt MySQL server connection. Assuming you are running MySQL
server with default setting (user 'root' with no password) */
$link = mysqli_connect("localhost", "root", "", "scandiweb");

// Check connection
if($link === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}

// Attempt insert query execution
$sql = "INSERT INTO items (sku, name, price, type, size) VALUES
            ('GGWP1100', 'Disc-EVO', '2', 'DVD','500'),
            ('GGWP1200', 'Disc-EVQ', '4', 'DVD','700'),
            ('GGWP1300', 'Disc-EVD', '8', 'DVD','1000')";
$sql_2 = "INSERT INTO items (sku, name, price, type, width, length, height) VALUES
            ('FRN800', 'TAABL', '280', 'Furniture','300','200','100'),
            ('FRN900', 'CHIIR', '42', 'Furniture','150','150','100'),
            ('FRN1000', 'LAAMP', '140', 'Furniture','50','50','50')";
$sql_3 = "INSERT INTO items (sku, name, price, type, weight) VALUES
            ('BBK100', 'Wizard Of Oz', '12', 'Book','1.2'),
            ('BBK120', 'Going UP!', '14', 'Book','1.4'),
            ('BBK130', 'To Kill A Mockingbird', '16', 'Book','1')";

if(mysqli_query($link, $sql)){
    echo "Records added successfully.";
} else{
    echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
}
if(mysqli_query($link, $sql_2)){
    echo "Records added successfully.";
} else{
    echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
}

if(mysqli_query($link, $sql_3)){
    echo "Records added successfully.";
} else{
    echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
}


// Close connection
mysqli_close($link);
?>