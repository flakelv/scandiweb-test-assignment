<?php
abstract class Product {
    public $sku = null;
    public $name = null;
    public $price = null;

    abstract public function getType();

    public function __construct($sku, $name, $price) {
        $this->setSKU($sku);
        $this->setName($name);
        $this->setPrice($price);
    }

    /** SKU **/
    final public function setSKU($sku): void {
        $this->sku = $sku;
    }
    final public function getSKU(): string {
        return $this->sku;
    }
    /** Name **/
    final public function setName($name): void {
        $this->name = $name;
    }
    final public function getName(): string {
        return $this->name;
    }
    /** Price **/
    final public function setPrice($price): void {
        $this->price = $price;
    }
    final public function getPrice(): string {

        return $this->price;
    }
}

class DVD extends Product {
    protected $type = 'DVD';
    public $size = null;

    public function __construct($sku, $name, $price, $size) {
        parent::__construct($sku, $name, $price);
        $this->setSize($size);
    }

    final public function getType(): string {
        return $this->type;
    }
    /** Size **/
    final public function setSize($size): void {
        $this->size = $size;
    }
    final public function getSize(): string {
        return $this->size;
    }
};

class Furniture extends Product {
    protected $type = 'Furniture';
    public $height = null;
    public $width = null;
    public $length = null;

    public function __construct($sku, $name, $price, $height, $width, $length) {
        parent::__construct($sku, $name, $price);
        $this->setHeight($height);
        $this->setWidth($width);
        $this->setLength($length);
    }

    final public function getType(): string {
        return $this->type;
    }
    /** Height **/
    final public function setHeight($height): void {
        $this->height = $height;
    }
    final public function getHeight(): string {
        return $this->height;
    }
    /** Width **/
    final public function setWidth($width): void {
        $this->width = $width;
    }
    final public function getWidth(): string {
        return $this->width;
    }
    /** Length **/
    final public function setLength($length): void {
        $this->length = $length;
    }
    final public function getLength(): string {
        return $this->length;
    }
};

class Book extends Product {
    protected $type = 'Book';
    public $weight = null;

    public function __construct($sku, $name, $price, $weight) {
        parent::__construct($sku, $name, $price);
        $this->setWeight($weight);
    }

    final public function getType(): string {
        return $this->type;
    }
    /** Weight **/
    final public function setWeight($weight): void {
        $this->weight = $weight;
    }
    final public function getWeight(): string {
        return $this->weight;
    }
}
?>